# AT_report
## Name
AT_report: Activity tracking reports
## Description
This image is meant to read the last 'Unseen' email from marcos.ibanez254@outlook.com. Send back a filtered copy to any address identified in the config file. It will continue to check every 20 minutes through a sleep command. If no email has not been read yet, then the program will do nothing.   

 This is a temporary email. Currently it is set up to receive any emails sent to the Schneider Electric group with the activity tracking report as the title. This happens by forwarding the email from Marcos.ibanez@vimaanrobotics.com.  

## Installation
I have not installed an image from Gitlab. 
Once the image is downloaded, move to the dockerfile location.  
If no image is created then do:  
Creating image  
sudo docker build --tag at_report .  
at_report being any tag you choose.   

## Usage
Run the docker image  
sudo docker run at_report .  
Add, to run in background  
tail -f /dev/null  
Or,  
sudo docker run -d at_report  

This can be ran without docker, by just using the python/config files.   
Everything that needs to be downlaoded is also in the requirements file.   
Can be ran with python AT_report.py

## How it works
A new email from wendy/vicent is sent to SE group. My personal email will forward these emails to the email address marcos.ibanez254@outlook.com which is a temp email. This is not in the program.  
Once the email arrives in temp email. The program is searching for an unread email with an attachment. This loops every 20 minutes. If no email is found, then the program does nothing. If no attachement is found with activity tracking then nothing happens.  
Once a correct attachement with .xlsx is found, it downloads the file, and saves it into a folder within the docker image.  
Program will then search through the folder and find the activity tracking report that matches with the one downloaded.  
Gets the file and begins to filter for specific columns and pickers. These can be changed in the config file.  
Once the excel is filtered, the excel will be sent out to a specifiec email, which can be modified in the config file.   
The running command is written in the dockerfile.  
CMD python3 at_report.py  
## Project status
Done

